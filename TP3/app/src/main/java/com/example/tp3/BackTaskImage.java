package com.example.tp3;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.os.AsyncTask;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class BackTaskImage extends AsyncTask<String, Void, Void>{

    ImageView img;
    Activity activity;

    public BackTaskImage(ImageView img,Activity activity){
        this.img = img;
        this.activity = activity;
    }

    @SuppressLint("WrongThread")
    @Override
    protected Void doInBackground(String... strings) {
        InputStream is;
        Bitmap bit = null;
        try {
            is = (InputStream) new URL(strings[0]).getContent();
            bit = BitmapFactory.decodeStream(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        final Bitmap finalBit = bit;
        this.activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                img.setImageBitmap(finalBit);
            }
        });
        return null;
    }

}
