package com.example.tp3;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;


public class TeamAdapter extends RecyclerView.Adapter<TeamAdapter.ViewHolder> {

    private static final String TAG = "WineListAdapter";
    private Context aContext;
    private static ArrayList<Team> TeamL = new ArrayList<>();
    private SportDbHelper SportDB;
    private static TeamAdapter singleton;
    private Activity activity;

    public TeamAdapter(Context context, SportDbHelper SDB, Activity activity){
        aContext = context;
        SportDB = SDB;
        this.activity = activity;
        Cursor cursor = SDB.fetchAllTeams();
        while(!cursor.isAfterLast()) {
            TeamL.add(SportDbHelper.cursorToTeam(cursor));
            cursor.moveToNext();
        }
    }

    public static  TeamAdapter get(Context context, SportDbHelper SDB, Activity activity){
        if(singleton == null) {
            singleton = new TeamAdapter(context,SDB,activity);
        }
        return singleton;
    }

    public static ArrayList<Team> getListe(){
        return TeamL;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_team, parent,false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        holder.nom.setText(TeamL.get(position).getName());
        holder.ligue.setText(TeamL.get(position).getLeague());
        new BackTaskImage(holder.img, activity).execute(TeamL.get(position).getTeamBadge());
        holder.pLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(aContext,TeamActivity.class);
                intent.putExtra("team", TeamL.get(position));
                intent.putExtra("position", position);
                aContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return TeamL.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView nom;
        TextView ligue;
        ImageView img;
        RelativeLayout pLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            nom = itemView.findViewById(R.id.team);
            ligue = itemView.findViewById(R.id.ligue);
            img = itemView.findViewById(R.id.img);
            pLayout = itemView.findViewById(R.id.pLayout);
        }
    }
}
