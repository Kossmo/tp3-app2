package com.example.tp3;

import android.util.JsonReader;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Date;


public class JSONResponseHandlerRanking {

    private static final String TAG = JSONResponseHandlerTeam.class.getSimpleName();

    private Team team;


    public JSONResponseHandlerRanking(Team team) {
        this.team = team;
    }

    public Team getTeam(){ return this.team;}


    public void readJsonStream(InputStream response) throws IOException {
        JsonReader reader = new JsonReader(new InputStreamReader(response, "UTF-8"));
        try {
            readTeams(reader);
        } finally {
            reader.close();
        }
    }

    public void readTeams(JsonReader reader) throws IOException {
        reader.beginObject();
        while (reader.hasNext()) {
            String name = reader.nextName();
            if (name.equals("table")) {
                readArrayTeams(reader);
            } else {
                reader.skipValue();
            }
        }
        reader.endObject();
    }


    private void readArrayTeams(JsonReader reader) throws IOException {
        reader.beginArray();
        int nb = 0;
        while (reader.hasNext() ) {
            reader.beginObject();
            boolean bonneTeam = false;
            while (reader.hasNext()) {
                String name = reader.nextName();
                if (name.equals("teamid")) {
                    String idT = reader.nextString();
                    if (Long.valueOf(idT) == team.getIdTeam()){
                        team.setRanking(nb);
                        bonneTeam = true;
                    }
                } else if (name.equals("total") && bonneTeam) {
                    team.setTotalPoints(reader.nextInt());
                    bonneTeam = false;
                } else {
                    reader.skipValue();
                }
            }
            reader.endObject();
            nb++;
        }
        reader.endArray();
    }

}
