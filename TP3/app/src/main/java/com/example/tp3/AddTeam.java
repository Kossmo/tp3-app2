package com.example.tp3;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.nfc.Tag;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class AddTeam extends AppCompatActivity {

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_team);

       setValue();
    }

    private void setValue(){
        final TextView add_nom = findViewById(R.id.editNewName);
        final TextView add_ligue = findViewById(R.id.editNewLeague);
        Button button = findViewById(R.id.button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String nom = String.valueOf(add_nom.getText());
                String ligue = String.valueOf(add_ligue.getText());
                if (nom.length() != 0 && ligue.length() != 0){
                    Team nTeam = new Team(nom,ligue);
                    SportDbHelper SDH = new SportDbHelper(AddTeam.this);
                    SDH.addTeam(nTeam);
                    TeamAdapter.get(getApplicationContext(),SDH, AddTeam.this).getListe().add(nTeam);
                    TeamAdapter.get(getApplicationContext(),SDH, AddTeam.this).notifyDataSetChanged();
                    finish();
                } else {
                    AlertDialog.Builder popup = new AlertDialog.Builder(AddTeam.this);
                    popup.setTitle("Sauvegarde Impossible");
                    popup.setMessage("Toutes les entrées  doivent être remplis");
                    popup.show();
                }
            }
        });
    }

}
